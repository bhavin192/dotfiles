# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# BEGIN: snippet from BLFS (MIT License)
# http://www.linuxfromscratch.org/blfs/view/svn/legalnotice.html
# http://www.linuxfromscratch.org/blfs/view/svn/postlfs/profile.html#etc-profile-profile

# Written for Beyond Linux From Scratch
# by James Robertson <jameswrobertson@earthlink.net>
# modifications by Dagmar d'Surreal <rivyqntzne@pbzpnfg.arg>

# Functions to help us manage paths.  Second argument is the name of the
# path variable to be modified (default: PATH)
pathremove () {
    local IFS=':'
    local NEWPATH
    local DIR
    local PATHVARIABLE=${2:-PATH}
    for DIR in ${!PATHVARIABLE} ; do
        if [ "$DIR" != "$1" ] ; then
            NEWPATH=${NEWPATH:+$NEWPATH:}$DIR
        fi
    done
    export $PATHVARIABLE="$NEWPATH"
}

pathprepend () {
    pathremove $1 $2
    local PATHVARIABLE=${2:-PATH}
    export $PATHVARIABLE="$1${!PATHVARIABLE:+:${!PATHVARIABLE}}"
}

pathappend () {
    pathremove $1 $2
    local PATHVARIABLE=${2:-PATH}
    export $PATHVARIABLE="${!PATHVARIABLE:+${!PATHVARIABLE}:}$1"
}
# END: snippet from BLFS (MIT License)

# User specific environment
pathprepend "${HOME}/.local/bin"

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

PROMPT_COMMAND=__prompt_command

# ref: https://stackoverflow.com/a/16715681/6202405
__prompt_command() {
    local EXIT_CODE="$?"
    # required if using Tilix as terminal
    if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        __vte_prompt_command
    fi
    local RCol='\[\e[0m\]'
    local BRed='\[\e[1;31m\]'
    local BGre='\[\e[1;32m\]'
    local BYel='\[\e[1;33m\]'
    local BBlu='\[\e[1;34m\]'

    PS1=""
    if [[ $EXIT_CODE -ne 0 ]]; then
        PS1+="${BRed}$EXIT_CODE${RCol}|"
    fi

    if [[ ! -z $VIRTUAL_ENV ]]; then
        PS1+="($(echo "$VIRTUAL_ENV" | rev | cut -d'/' -f1 | rev)) "
    fi

    if [[ -n $SHOW_KUBE_CTX ]]; then
        PS1+="${BBlu}(⎈ $(kubectl config current-context))${RCol} "
    fi

    PS1+="${BGre}\u${RCol}"
    PS1+="@\h${RCol}"
    PS1+=" \$(_dir_chomp '$(pwd)' 20) "
    PS1+="${BYel}$(parse_git_branch)${RCol} "
    PS1+="\t $ "
}

# ref: https://stackoverflow.com/a/3499237
_dir_chomp () {
    local p=${1/#$HOME/\~} b s
    s=${#p}
    while [[ $p != "${p//\/}" ]]&&(($s>$2))
    do
        p=${p#/}
        [[ $p =~ \.?. ]]
        b=$b/${BASH_REMATCH[0]}
        p=${p#*/}
        ((s=${#b}+${#p}))
    done
    echo ${b/\/~/\~}${b+/}$p
}

# ref: https://stackoverflow.com/q/3497885/#comment3654783_3497885
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[⎇ \1]/'
}
# git branch symbols: -∠  ⎇ ⛕
# ref: https://git.io/fpUEQ

# Kubectl shell completion
if type kubectl &>/dev/null; then
    source <(kubectl completion bash)
fi

# Go config
export GOPATH="${HOME}/work/go"
pathappend "${HOME}/work/go/bin"

# GNU Emacs related config
if [[ -z $INSIDE_EMACS ]]; then
    export EDITOR="emacsclient -t"
else
    export EDITOR="emacsclient"
fi
alias emc="emacsclient"

# ping alias commands
# ref: https://twitter.com/SatKanetkar/status/927776154731384832
alias pingoo="ping 8.8.8.8"
alias pingcf="ping 1.1.1.1"

# Other configs
# export TERM="xterm-256color"

# Local bin for npm packages see .npmrc
pathappend "${HOME}/.npm/bin"

# Source extra configurations
if [[ -f "${HOME}/.config/bhavin/bash-config" ]]; then
    source ${HOME}/.config/bhavin/bash-config
fi

