;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
;; Added by customize-themes
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(spacemacs-dark))
 '(custom-safe-themes
   '("fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default))
 '(package-selected-packages
   '(speed-type org-tree-slide vterm activity-watch-mode magit browse-kill-ring golden-ratio lsp-java notmuch kubel git-link nyan-mode which-key ox-slack lsp-ui lsp-mode terraform-mode htmlize ox-reveal bash-completion dockerfile-mode elfeed elpy go-mode groovy-mode ledger-mode markdown-mode org-scrum spaceline spacemacs-theme undo-tree yaml-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Disable tool-bar scroll-bar and menu-bar
(tool-bar-mode -1)
(scroll-bar-mode -1)
;; (menu-bar-mode -1)
;; Display battery percentage in mode line
(display-battery-mode 1)
;; Path to add custom .el files
(add-to-list 'load-path "~/.emacs.d/lib/")
;; MELPA package repository
(add-to-list 'package-archives
             '("melpa-edge" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;; Load modes from lib
;; (require 'yaml-mode)
;; Requires gnuplot package installed on the system
;; (require 'scrum)

;; Binding for Magit
(global-set-key (kbd "C-x g") 'magit-status)
;; Spaceline config
(require 'spaceline-config)
(spaceline-spacemacs-theme)
;; elpy-mode settings
(elpy-enable)
;; Enable undo-tree in all buffers
(global-undo-tree-mode)

;; Org mode customizations
;; ref: https://gitlab.com/shakthimaan/cask-dot-emacs/blob/master/emacs-init.org#L259-351
;; Add closed timestamp when task is marked DONE
(setq org-log-done 'time)
;; Add notes at the end of all drawers
(setq org-log-state-notes-insert-after-drawers t)
;; Keep Plain Lists folded
(setq org-cycle-include-plain-lists 'integrate)
(global-set-key (kbd "C-c l") 'org-store-link)

(setq org-todo-keywords
    '((sequence "TODO(t)" "NEXT(n)" "IN_PROGRESS(p)" "WAITING(w)" "|" "DONE(d)" "CANCELED(c@)")))
;; Function and binding to update the ACTUAL in org file
;; workaround for missing org-clock-sum-current-item on Fedora 28/29
(require 'org-clock)
(defun org-update-actuals ()
    (interactive)
    (org-entry-put (point) "ACTUAL"
      (format "%0.2f" (/ (org-clock-sum-current-item) 60.0))))

(define-skeleton insert-org-entry
  "Prompt for task, estimate and category"
  nil
  '(setq task  (skeleton-read "Task: "))
  '(setq estimate  (skeleton-read "Estimate: "))
  '(setq category (skeleton-read "Category: "))
  '(setq timestamp (format-time-string "%s"))
  "** " task \n
  ":PROPERTIES:" \n
  ":ESTIMATED: " estimate \n
  ":ACTUAL:" \n
  ":OWNER: bhavin192" \n
  ":ID: " category "." timestamp \n
  ":TASKID: " category "." timestamp \n
  ":END:")

(global-set-key (kbd "C-c s c") 'org-update-actuals)
(global-set-key (kbd "C-c s u") 'org-scrum-update-all)

;; Org Agenda
(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-agenda-files '("~/work/orgfiles"
			 "~/src/orgfiles"))
(setq org-agenda-span 'day)
;; Per day clock report in Org agenda
(setq org-agenda-clockreport-parameter-plist
  '(:link t :maxlevel 3 :fileskip0 t :stepskip0 t :step day))

;; Custom commands to filter based on tags
(setq org-agenda-custom-commands
      '(("w" "Work tasks" agenda "" ((org-agenda-tag-filter-preset '("-personal"))))
	("p" "Personal tasks" agenda "" ((org-agenda-tag-filter-preset '("+personal"))))))

;; Org Capture
(global-set-key (kbd "C-c c") 'org-capture)

;; Capture templates for: TODO tasks, Notes
(setq org-capture-templates '(
  ("t" "Todo" entry (file "~/src/orgfiles/refile.org")
       "* TODO %^{Description}\n  :PROPERTIES:\n  :CREATED:  %U\n  :END:\n  %i%?\n" :prepend t)
  ("n" "Note" entry (file+headline "~/src/orgfiles/personal.org" "Notes")
       "* %^{Description}\n  :PROPERTIES:\n  :CREATED:  %U\n  :END:\n  %i%?\n" :prepend t)
  ("i" "Interview" entry (file+headline "~/src/orgfiles/InfraCloud.org" "Interviews")
       "* TODO %^{Role|SRE|} - %^{Name}\n  SCHEDULED: %^T\n  :PROPERTIES:\n  :CREATED:  %U\n  :END:\n  - %^{Platform link}%?\n"
       :prepend t :immediate-finish t)
  ("p" "Protocol Note" entry (file+headline "~/src/orgfiles/personal.org" "Notes")
       "* %?%:description\n  :PROPERTIES:\n  :CREATED:  %U\n  :END:\n  #+BEGIN_QUOTE\n  %i\n  #+END_QUOTE\n  - [[%:link][%:description]]\n" :prepend t)
))

;; Org protocol for Org Capture extension
(require 'org-protocol)

;; Org refile
(setq org-refile-targets '((org-agenda-files :maxlevel . 2)))
(setq org-refile-use-outline-path 'file)
;; Prepend the entry while doing refile
(setq org-reverse-note-order t)

;; Org Habits
(require 'org-habit)
;; (add-to-list 'org-modules 'org-habits)
;; (setq org-modules '(org-habits))
(setq org-habit-graph-column 60)

;; For the RESET_CHECK_BOXES property
;; https://orgmode.org/worg/org-contrib/org-checklist.html
(require 'org-checklist)

;; Elfeed
(global-set-key (kbd "C-x w") 'elfeed)
(setq elfeed-feeds
      '("https://blog.golang.org/feed.atom"
	"https://kubernetes.io/feed.xml"
	"https://seths.blog/feed"
	"https://janusworx.com/rss.xml"
	"https://medium.com/feed/jaegertracing"
	"https://geeksocket.in/index.xml"
	"https://fedoramagazine.org/feed"
	("https://news.kushaldas.in/feed/" newsletter)
	("https://sachachua.com/blog/category/emacs-news/feed" newsletter)
	"https://masteringemacs.org/feed"))

;; Shell-script
(setq sh-basic-offset 2)

;; Company mode
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)

;; Go - lsp-mode
;; Set up before-save hooks to format buffer and add/delete imports.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)

;; Ibuffer grouped by major-mode
(global-set-key (kbd "C-x C-b") 'ibuffer)
(add-hook 'ibuffer-mode-hook #'ibuffer-set-filter-groups-by-mode)

;; Enable which-key mode
(which-key-mode)

;; password-store keybindings
(global-set-key (kbd "C-c p i") 'password-store-insert)
(global-set-key (kbd "C-c p e") 'password-store-edit)
(global-set-key (kbd "C-c p y") 'password-store-copy)
(global-set-key (kbd "C-c p f") 'password-store-copy-field)
(global-set-key (kbd "C-c p b") 'password-store-url)

;; Add bash-completion to shell
(add-hook 'shell-dynamic-complete-functions
     'bash-completion-dynamic-complete)

;; From crux: https://github.com/bbatsov/crux/blob/master/crux.el
(defun crux-rename-file-and-buffer ()
  "Rename current buffer and if the buffer is visiting a file, rename it too."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
	(rename-buffer (read-from-minibuffer "New name: " (buffer-name)))
      (let* ((new-name (read-from-minibuffer "New name: " filename))
	     (containing-dir (file-name-directory new-name)))
	(make-directory containing-dir t)
	(cond
	 ((vc-backend filename) (vc-rename-file filename new-name))
	 (t
	  (rename-file filename new-name t)
	  (set-visited-file-name new-name t t)))))))

(global-set-key (kbd "C-c r")  #'crux-rename-file-and-buffer)

;; Git Link
(setq git-link-use-commit t)
(setq git-link-open-in-browser t)
(global-set-key (kbd "C-c g l") #'git-link)
(global-set-key (kbd "C-c g c") #'git-link-commit)
(global-set-key (kbd "C-c g h") #'git-link-homepage)

(golden-ratio-mode t)

;; Start server mode
(server-start)

;; Maximize the frame
(toggle-frame-maximized)
