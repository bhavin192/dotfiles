# Collection of dotfiles

This repository contains some of the dotfiles which I use on my workstations.

## GNU Emacs [.emacs]
- After placing the file at `~/.emacs` make sure you install following packages with

  ```
  M-x package-refresh-contents
  M-x package-install RET <package-name>
  ```

  - bash-completion
  - dockerfile-mode
  - elfeed
  - elpy
  - epresent
  - go-mode
  - groovy-mode
  - ledger-mode
  - magit
  - markdown-mode
  - org-scrum
  - spaceline
  - spacemacs-theme
  - undo-tree
  - yaml-mode
- Create a directory `~/.emacs.d/lib` and put following `.el` files in the directory
  - [`ox-confluence.el`](https://code.orgmode.org/bzg/org-mode/raw/release_9.1.9/contrib/lisp/ox-confluence.el)
  - [`ox-confluence-en.el`](https://raw.githubusercontent.com/correl/ox-confluence-en/master/ox-confluence-en.el)

## Bash configuration [.bashrc]
This sets `PROMPT_COMMAND` variable to a function which sets the variable `PS1`
- Shows current git branch
- Shows exit code of command if it's non zero
- Shows name of Python Virtual Environment when active

![bash prompt](https://paste.opensuse.org/view/raw/8366121)

## Licensing
dotfiles is licensed under GNU General Public License v3.0. See [LICENSE](./LICENSE) for the full license text.
